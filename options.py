#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/12
    Time: 10:50
"""
from __future__ import absolute_import

from tornado.options import define
from tornado.options import options

DEFAULT_CONFIG_FILE = "config.py"

define("port", default=5555, help="run on the given port", type=int)
define("address", default='', help="run on the give address", type=str)
define("debug", default=False, help="run in debug mode", type=bool)

default_options = options
