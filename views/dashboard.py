#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/13
    Time: 11:41
"""

from __future__ import absolute_import
import logging
from tornado import web
from tornado import gen
from tornado import websocket
from tornado.ioloop import PeriodicCallback

from . import BaseHandler

class DashBoardView(BaseHandler):
    # @web.authenticated
    @gen.coroutine
    def get(self, *args, **kwargs):
        args = {
            "pageTitle":"任务列表",
            "siteName":"任务调度系统",
            "version" :"0.0.1",
            "userName":"Atom",
        }
        self.render("layout/layout.html", **args)
