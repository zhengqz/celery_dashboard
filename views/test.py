#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/13
    Time: 11:06
"""

from __future__ import absolute_import
from . import BaseHandler

class TestView(BaseHandler):
    def get(self, *args, **kwargs):
        self.write("Test OK!")