#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
    Date: 2016/9/13
    Time: 10:55
"""

from __future__ import absolute_import
from __future__ import print_function

from celery.bin.base import Command

class BaseCommand(Command):
    def run_from_argv(self, prog_name, argv=None, command=None):
        self.apply_env_options()
        self.apply_options(prog_name, argv)


    def run(self):
        self.run_from_argv()
