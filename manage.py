#!/usr/bin/env python
# -*- coding:utf-8 -*-

from __future__ import absolute_import
from __future__ import print_function

import celery
import tornado.ioloop
import tornado.httpserver
import tornado.web
import tornado.options

from options import default_options
from urls import handlers, settings



class CeleryApp(tornado.web.Application):
    def __init__(self, options=None, capp=None, events=None, io_loop=None, **kwargs):
        kwargs.update(handlers=handlers,**settings)

        super(CeleryApp, self).__init__(**kwargs)
        self.options = options or default_options
        self.io_loop = io_loop or tornado.ioloop.IOLoop.instance()
        self.ssl_options = kwargs.get("ssl_options", None)
        self.capp = capp or celery.Celery()


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = CeleryApp()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(app.options.port)
    app.io_loop.start()


